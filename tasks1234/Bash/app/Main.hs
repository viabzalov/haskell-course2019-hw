{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE DeriveGeneric      #-}
{-# LANGUAGE OverloadedStrings  #-}
{-# LANGUAGE QuasiQuotes        #-}
{-# LANGUAGE RecordWildCards    #-}

module Main where

import qualified Data.Text                  as T
import qualified Data.Text.IO               as TIO
import           Language.Bash.AST
import           Language.Bash.Interpreter
import           Language.Bash.Parser
import           Language.Bash.Pretty
import           System.Environment
import           System.FilePath.Posix
import           Text.InterpolatedString.QM
import           Text.Megaparsec            hiding (State)
import           Text.Pretty.Simple         (pPrint)

main = do
    -- | example1: 'stack exec Bash-exe app/1.sh 2 5'
    -- | example2: 'stack exec Bash-exe app/factorial.sh 10'
    -- | example3: 'stack exec Bash-exe' -- interactive mode
    arguments <- getArgs
    case arguments of
        [] -> interactiveMode []
        (name:args) -> do
            test <- readFile name
            writeFile ((fst . splitExtensions $ name) ++ "_pretty" ++ (snd . splitExtensions $ name)) (prExprString args test)
            iExprString args test

interactiveMode vars = do
    command <- getLine
    vars' <- iExprText vars (T.pack command)
    interactiveMode vars'
